package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.marker.IntegrationCategory;


import java.util.List;

public class TaskEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("test", "test");
        endpointLocator.getTaskEndpoint().clearTasks(session);
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void addTaskTest() {
        final String taskName = "nameTest";
        final String taskDescription = "nameTest";
        endpointLocator.getTaskEndpoint().addTask(taskName, taskDescription, session);
        final Task task = endpointLocator.getTaskEndpoint().findTaskByName(taskName, session);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getDescription());
        Assert.assertEquals(taskDescription, task.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllTaskTest() {
        endpointLocator.getTaskEndpoint().addTask("task1", "description1", session);
        endpointLocator.getTaskEndpoint().addTask("task2", "description2", session);
        endpointLocator.getTaskEndpoint().addTask("task3", "description3", session);
        Assert.assertEquals(3, endpointLocator.getTaskEndpoint().findAllTasks(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskByIdTest() {
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findAllTasks(session).size());
        endpointLocator.getTaskEndpoint().addTask("AA", "BB", session);
        List<Task> taskList = endpointLocator.getTaskEndpoint().findAllTasks(session);
        final Task task = taskList.get(0);
        final Task taskFound = endpointLocator.getTaskEndpoint().findTaskById(task.id, session);
        Assert.assertEquals("AA", taskFound.name);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByIndexTest() {
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findAllTasks(session).size());
        endpointLocator.getTaskEndpoint().addTask("AA", "BB", session);
        final Task taskFound = endpointLocator.getTaskEndpoint().findTaskByIndex(0, session);
        Assert.assertEquals("AA", taskFound.getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByNameTest() {
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findAllTasks(session).size());
        endpointLocator.getTaskEndpoint().addTask("AA", "BB", session);
        final Task taskFound = endpointLocator.getTaskEndpoint().findTaskByName("AA", session);
        Assert.assertEquals("AA", taskFound.getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskOneByIdTest() {
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findAllTasks(session).size());
        endpointLocator.getTaskEndpoint().addTask("AA", "BB", session);
        List<Task> taskList = endpointLocator.getTaskEndpoint().findAllTasks(session);
        final Task task = taskList.get(0);
        endpointLocator.getTaskEndpoint().removeTaskById(task.getId(), session);
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findAllTasks(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskByNameTest() {
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findAllTasks(session).size());
        endpointLocator.getTaskEndpoint().addTask("AA", "BB", session);
        endpointLocator.getTaskEndpoint().removeTaskByName("AA", session);
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findAllTasks(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskByIndexTest() {
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findAllTasks(session).size());
        endpointLocator.getTaskEndpoint().addTask("AA", "BB", session);
        endpointLocator.getTaskEndpoint().removeTaskByIndex(0, session);
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findAllTasks(session).size());
    }

}
