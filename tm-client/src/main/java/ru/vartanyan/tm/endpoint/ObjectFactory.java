
package ru.vartanyan.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.vartanyan.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AccessDeniedException_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "AccessDeniedException");
    private final static QName _EmptyIdException_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "EmptyIdException");
    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "Exception");
    private final static QName _AddAllUsers_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "addAllUsers");
    private final static QName _AddAllUsersResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "addAllUsersResponse");
    private final static QName _AddUser_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "addUser");
    private final static QName _AddUserResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "addUserResponse");
    private final static QName _ClearUsers_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "clearUsers");
    private final static QName _ClearUsersResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "clearUsersResponse");
    private final static QName _CreateUser_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "createUser");
    private final static QName _CreateUserResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "createUserResponse");
    private final static QName _CreateUserWithEmail_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "createUserWithEmail");
    private final static QName _CreateUserWithEmailResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "createUserWithEmailResponse");
    private final static QName _CreateUserWithRole_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "createUserWithRole");
    private final static QName _CreateUserWithRoleResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "createUserWithRoleResponse");
    private final static QName _FindAllUsers_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "findAllUsers");
    private final static QName _FindAllUsersResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "findAllUsersResponse");
    private final static QName _FindUserById_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "findUserById");
    private final static QName _FindUserByIdResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "findUserByIdResponse");
    private final static QName _FindUserByItsLogin_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "findUserByItsLogin");
    private final static QName _FindUserByItsLoginResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "findUserByItsLoginResponse");
    private final static QName _LockUserById_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "lockUserById");
    private final static QName _LockUserByIdResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "lockUserByIdResponse");
    private final static QName _LockUserByLogin_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "lockUserByLogin");
    private final static QName _LockUserByLoginResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "lockUserByLoginResponse");
    private final static QName _RemoveUser_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "removeUser");
    private final static QName _RemoveUserById_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "removeUserById");
    private final static QName _RemoveUserByIdResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "removeUserByIdResponse");
    private final static QName _RemoveUserByLogin_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "removeUserByLogin");
    private final static QName _RemoveUserByLoginResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "removeUserByLoginResponse");
    private final static QName _RemoveUserResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "removeUserResponse");
    private final static QName _SetUserPassword_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "setUserPassword");
    private final static QName _SetUserPasswordResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "setUserPasswordResponse");
    private final static QName _UnlockUserById_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "unlockUserById");
    private final static QName _UnlockUserByIdResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "unlockUserByIdResponse");
    private final static QName _UnlockUserByLogin_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "unlockUserByLogin");
    private final static QName _UnlockUserByLoginResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "unlockUserByLoginResponse");
    private final static QName _UpdateUser_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "updateUser");
    private final static QName _UpdateUserResponse_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "updateUserResponse");
    private final static QName _YourRootElementName_QNAME = new QName("http://endpoint.tm.vartanyan.ru/", "yourRootElementName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.vartanyan.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AccessDeniedException }
     * 
     */
    public AccessDeniedException createAccessDeniedException() {
        return new AccessDeniedException();
    }

    /**
     * Create an instance of {@link EmptyIdException }
     * 
     */
    public EmptyIdException createEmptyIdException() {
        return new EmptyIdException();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link AddAllUsers }
     * 
     */
    public AddAllUsers createAddAllUsers() {
        return new AddAllUsers();
    }

    /**
     * Create an instance of {@link AddAllUsersResponse }
     * 
     */
    public AddAllUsersResponse createAddAllUsersResponse() {
        return new AddAllUsersResponse();
    }

    /**
     * Create an instance of {@link AddUser }
     * 
     */
    public AddUser createAddUser() {
        return new AddUser();
    }

    /**
     * Create an instance of {@link AddUserResponse }
     * 
     */
    public AddUserResponse createAddUserResponse() {
        return new AddUserResponse();
    }

    /**
     * Create an instance of {@link ClearUsers }
     * 
     */
    public ClearUsers createClearUsers() {
        return new ClearUsers();
    }

    /**
     * Create an instance of {@link ClearUsersResponse }
     * 
     */
    public ClearUsersResponse createClearUsersResponse() {
        return new ClearUsersResponse();
    }

    /**
     * Create an instance of {@link CreateUser }
     * 
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link CreateUserWithEmail }
     * 
     */
    public CreateUserWithEmail createCreateUserWithEmail() {
        return new CreateUserWithEmail();
    }

    /**
     * Create an instance of {@link CreateUserWithEmailResponse }
     * 
     */
    public CreateUserWithEmailResponse createCreateUserWithEmailResponse() {
        return new CreateUserWithEmailResponse();
    }

    /**
     * Create an instance of {@link CreateUserWithRole }
     * 
     */
    public CreateUserWithRole createCreateUserWithRole() {
        return new CreateUserWithRole();
    }

    /**
     * Create an instance of {@link CreateUserWithRoleResponse }
     * 
     */
    public CreateUserWithRoleResponse createCreateUserWithRoleResponse() {
        return new CreateUserWithRoleResponse();
    }

    /**
     * Create an instance of {@link FindAllUsers }
     * 
     */
    public FindAllUsers createFindAllUsers() {
        return new FindAllUsers();
    }

    /**
     * Create an instance of {@link FindAllUsersResponse }
     * 
     */
    public FindAllUsersResponse createFindAllUsersResponse() {
        return new FindAllUsersResponse();
    }

    /**
     * Create an instance of {@link FindUserById }
     * 
     */
    public FindUserById createFindUserById() {
        return new FindUserById();
    }

    /**
     * Create an instance of {@link FindUserByIdResponse }
     * 
     */
    public FindUserByIdResponse createFindUserByIdResponse() {
        return new FindUserByIdResponse();
    }

    /**
     * Create an instance of {@link FindUserByItsLogin }
     * 
     */
    public FindUserByItsLogin createFindUserByItsLogin() {
        return new FindUserByItsLogin();
    }

    /**
     * Create an instance of {@link FindUserByItsLoginResponse }
     * 
     */
    public FindUserByItsLoginResponse createFindUserByItsLoginResponse() {
        return new FindUserByItsLoginResponse();
    }

    /**
     * Create an instance of {@link LockUserById }
     * 
     */
    public LockUserById createLockUserById() {
        return new LockUserById();
    }

    /**
     * Create an instance of {@link LockUserByIdResponse }
     * 
     */
    public LockUserByIdResponse createLockUserByIdResponse() {
        return new LockUserByIdResponse();
    }

    /**
     * Create an instance of {@link LockUserByLogin }
     * 
     */
    public LockUserByLogin createLockUserByLogin() {
        return new LockUserByLogin();
    }

    /**
     * Create an instance of {@link LockUserByLoginResponse }
     * 
     */
    public LockUserByLoginResponse createLockUserByLoginResponse() {
        return new LockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveUser }
     * 
     */
    public RemoveUser createRemoveUser() {
        return new RemoveUser();
    }

    /**
     * Create an instance of {@link RemoveUserById }
     * 
     */
    public RemoveUserById createRemoveUserById() {
        return new RemoveUserById();
    }

    /**
     * Create an instance of {@link RemoveUserByIdResponse }
     * 
     */
    public RemoveUserByIdResponse createRemoveUserByIdResponse() {
        return new RemoveUserByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveUserByLogin }
     * 
     */
    public RemoveUserByLogin createRemoveUserByLogin() {
        return new RemoveUserByLogin();
    }

    /**
     * Create an instance of {@link RemoveUserByLoginResponse }
     * 
     */
    public RemoveUserByLoginResponse createRemoveUserByLoginResponse() {
        return new RemoveUserByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveUserResponse }
     * 
     */
    public RemoveUserResponse createRemoveUserResponse() {
        return new RemoveUserResponse();
    }

    /**
     * Create an instance of {@link SetUserPassword }
     * 
     */
    public SetUserPassword createSetUserPassword() {
        return new SetUserPassword();
    }

    /**
     * Create an instance of {@link SetUserPasswordResponse }
     * 
     */
    public SetUserPasswordResponse createSetUserPasswordResponse() {
        return new SetUserPasswordResponse();
    }

    /**
     * Create an instance of {@link UnlockUserById }
     * 
     */
    public UnlockUserById createUnlockUserById() {
        return new UnlockUserById();
    }

    /**
     * Create an instance of {@link UnlockUserByIdResponse }
     * 
     */
    public UnlockUserByIdResponse createUnlockUserByIdResponse() {
        return new UnlockUserByIdResponse();
    }

    /**
     * Create an instance of {@link UnlockUserByLogin }
     * 
     */
    public UnlockUserByLogin createUnlockUserByLogin() {
        return new UnlockUserByLogin();
    }

    /**
     * Create an instance of {@link UnlockUserByLoginResponse }
     * 
     */
    public UnlockUserByLoginResponse createUnlockUserByLoginResponse() {
        return new UnlockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link UpdateUser }
     * 
     */
    public UpdateUser createUpdateUser() {
        return new UpdateUser();
    }

    /**
     * Create an instance of {@link UpdateUserResponse }
     * 
     */
    public UpdateUserResponse createUpdateUserResponse() {
        return new UpdateUserResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccessDeniedException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AccessDeniedException }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "AccessDeniedException")
    public JAXBElement<AccessDeniedException> createAccessDeniedException(AccessDeniedException value) {
        return new JAXBElement<AccessDeniedException>(_AccessDeniedException_QNAME, AccessDeniedException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyIdException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EmptyIdException }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "EmptyIdException")
    public JAXBElement<EmptyIdException> createEmptyIdException(EmptyIdException value) {
        return new JAXBElement<EmptyIdException>(_EmptyIdException_QNAME, EmptyIdException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddAllUsers }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddAllUsers }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "addAllUsers")
    public JAXBElement<AddAllUsers> createAddAllUsers(AddAllUsers value) {
        return new JAXBElement<AddAllUsers>(_AddAllUsers_QNAME, AddAllUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddAllUsersResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddAllUsersResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "addAllUsersResponse")
    public JAXBElement<AddAllUsersResponse> createAddAllUsersResponse(AddAllUsersResponse value) {
        return new JAXBElement<AddAllUsersResponse>(_AddAllUsersResponse_QNAME, AddAllUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "addUser")
    public JAXBElement<AddUser> createAddUser(AddUser value) {
        return new JAXBElement<AddUser>(_AddUser_QNAME, AddUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "addUserResponse")
    public JAXBElement<AddUserResponse> createAddUserResponse(AddUserResponse value) {
        return new JAXBElement<AddUserResponse>(_AddUserResponse_QNAME, AddUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearUsers }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearUsers }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "clearUsers")
    public JAXBElement<ClearUsers> createClearUsers(ClearUsers value) {
        return new JAXBElement<ClearUsers>(_ClearUsers_QNAME, ClearUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearUsersResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearUsersResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "clearUsersResponse")
    public JAXBElement<ClearUsersResponse> createClearUsersResponse(ClearUsersResponse value) {
        return new JAXBElement<ClearUsersResponse>(_ClearUsersResponse_QNAME, ClearUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmail }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "createUserWithEmail")
    public JAXBElement<CreateUserWithEmail> createCreateUserWithEmail(CreateUserWithEmail value) {
        return new JAXBElement<CreateUserWithEmail>(_CreateUserWithEmail_QNAME, CreateUserWithEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "createUserWithEmailResponse")
    public JAXBElement<CreateUserWithEmailResponse> createCreateUserWithEmailResponse(CreateUserWithEmailResponse value) {
        return new JAXBElement<CreateUserWithEmailResponse>(_CreateUserWithEmailResponse_QNAME, CreateUserWithEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithRole }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserWithRole }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "createUserWithRole")
    public JAXBElement<CreateUserWithRole> createCreateUserWithRole(CreateUserWithRole value) {
        return new JAXBElement<CreateUserWithRole>(_CreateUserWithRole_QNAME, CreateUserWithRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithRoleResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserWithRoleResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "createUserWithRoleResponse")
    public JAXBElement<CreateUserWithRoleResponse> createCreateUserWithRoleResponse(CreateUserWithRoleResponse value) {
        return new JAXBElement<CreateUserWithRoleResponse>(_CreateUserWithRoleResponse_QNAME, CreateUserWithRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUsers }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindAllUsers }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "findAllUsers")
    public JAXBElement<FindAllUsers> createFindAllUsers(FindAllUsers value) {
        return new JAXBElement<FindAllUsers>(_FindAllUsers_QNAME, FindAllUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUsersResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindAllUsersResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "findAllUsersResponse")
    public JAXBElement<FindAllUsersResponse> createFindAllUsersResponse(FindAllUsersResponse value) {
        return new JAXBElement<FindAllUsersResponse>(_FindAllUsersResponse_QNAME, FindAllUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "findUserById")
    public JAXBElement<FindUserById> createFindUserById(FindUserById value) {
        return new JAXBElement<FindUserById>(_FindUserById_QNAME, FindUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "findUserByIdResponse")
    public JAXBElement<FindUserByIdResponse> createFindUserByIdResponse(FindUserByIdResponse value) {
        return new JAXBElement<FindUserByIdResponse>(_FindUserByIdResponse_QNAME, FindUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByItsLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserByItsLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "findUserByItsLogin")
    public JAXBElement<FindUserByItsLogin> createFindUserByItsLogin(FindUserByItsLogin value) {
        return new JAXBElement<FindUserByItsLogin>(_FindUserByItsLogin_QNAME, FindUserByItsLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByItsLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserByItsLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "findUserByItsLoginResponse")
    public JAXBElement<FindUserByItsLoginResponse> createFindUserByItsLoginResponse(FindUserByItsLoginResponse value) {
        return new JAXBElement<FindUserByItsLoginResponse>(_FindUserByItsLoginResponse_QNAME, FindUserByItsLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LockUserById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "lockUserById")
    public JAXBElement<LockUserById> createLockUserById(LockUserById value) {
        return new JAXBElement<LockUserById>(_LockUserById_QNAME, LockUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LockUserByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "lockUserByIdResponse")
    public JAXBElement<LockUserByIdResponse> createLockUserByIdResponse(LockUserByIdResponse value) {
        return new JAXBElement<LockUserByIdResponse>(_LockUserByIdResponse_QNAME, LockUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LockUserByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "lockUserByLogin")
    public JAXBElement<LockUserByLogin> createLockUserByLogin(LockUserByLogin value) {
        return new JAXBElement<LockUserByLogin>(_LockUserByLogin_QNAME, LockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LockUserByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "lockUserByLoginResponse")
    public JAXBElement<LockUserByLoginResponse> createLockUserByLoginResponse(LockUserByLoginResponse value) {
        return new JAXBElement<LockUserByLoginResponse>(_LockUserByLoginResponse_QNAME, LockUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "removeUser")
    public JAXBElement<RemoveUser> createRemoveUser(RemoveUser value) {
        return new JAXBElement<RemoveUser>(_RemoveUser_QNAME, RemoveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "removeUserById")
    public JAXBElement<RemoveUserById> createRemoveUserById(RemoveUserById value) {
        return new JAXBElement<RemoveUserById>(_RemoveUserById_QNAME, RemoveUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "removeUserByIdResponse")
    public JAXBElement<RemoveUserByIdResponse> createRemoveUserByIdResponse(RemoveUserByIdResponse value) {
        return new JAXBElement<RemoveUserByIdResponse>(_RemoveUserByIdResponse_QNAME, RemoveUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "removeUserByLogin")
    public JAXBElement<RemoveUserByLogin> createRemoveUserByLogin(RemoveUserByLogin value) {
        return new JAXBElement<RemoveUserByLogin>(_RemoveUserByLogin_QNAME, RemoveUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "removeUserByLoginResponse")
    public JAXBElement<RemoveUserByLoginResponse> createRemoveUserByLoginResponse(RemoveUserByLoginResponse value) {
        return new JAXBElement<RemoveUserByLoginResponse>(_RemoveUserByLoginResponse_QNAME, RemoveUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "removeUserResponse")
    public JAXBElement<RemoveUserResponse> createRemoveUserResponse(RemoveUserResponse value) {
        return new JAXBElement<RemoveUserResponse>(_RemoveUserResponse_QNAME, RemoveUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetUserPassword }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SetUserPassword }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "setUserPassword")
    public JAXBElement<SetUserPassword> createSetUserPassword(SetUserPassword value) {
        return new JAXBElement<SetUserPassword>(_SetUserPassword_QNAME, SetUserPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetUserPasswordResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SetUserPasswordResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "setUserPasswordResponse")
    public JAXBElement<SetUserPasswordResponse> createSetUserPasswordResponse(SetUserPasswordResponse value) {
        return new JAXBElement<SetUserPasswordResponse>(_SetUserPasswordResponse_QNAME, SetUserPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnlockUserById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "unlockUserById")
    public JAXBElement<UnlockUserById> createUnlockUserById(UnlockUserById value) {
        return new JAXBElement<UnlockUserById>(_UnlockUserById_QNAME, UnlockUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnlockUserByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "unlockUserByIdResponse")
    public JAXBElement<UnlockUserByIdResponse> createUnlockUserByIdResponse(UnlockUserByIdResponse value) {
        return new JAXBElement<UnlockUserByIdResponse>(_UnlockUserByIdResponse_QNAME, UnlockUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnlockUserByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "unlockUserByLogin")
    public JAXBElement<UnlockUserByLogin> createUnlockUserByLogin(UnlockUserByLogin value) {
        return new JAXBElement<UnlockUserByLogin>(_UnlockUserByLogin_QNAME, UnlockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnlockUserByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "unlockUserByLoginResponse")
    public JAXBElement<UnlockUserByLoginResponse> createUnlockUserByLoginResponse(UnlockUserByLoginResponse value) {
        return new JAXBElement<UnlockUserByLoginResponse>(_UnlockUserByLoginResponse_QNAME, UnlockUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "updateUser")
    public JAXBElement<UpdateUser> createUpdateUser(UpdateUser value) {
        return new JAXBElement<UpdateUser>(_UpdateUser_QNAME, UpdateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "updateUserResponse")
    public JAXBElement<UpdateUserResponse> createUpdateUserResponse(UpdateUserResponse value) {
        return new JAXBElement<UpdateUserResponse>(_UpdateUserResponse_QNAME, UpdateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vartanyan.ru/", name = "yourRootElementName")
    public JAXBElement<Object> createYourRootElementName(Object value) {
        return new JAXBElement<Object>(_YourRootElementName_QNAME, Object.class, null, value);
    }

}
