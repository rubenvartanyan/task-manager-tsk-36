package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.endpoint.Project;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.enumerated.Sort;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[PROJECT LIST]");
        List<Project> list;
        list = endpointLocator.getProjectEndpoint().findAllProjects(session);
        int index = 1;
        for (final Project project: list) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}
