package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove task by index";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[REMOVE TASK");
        System.out.println("[ENTER INDEX");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        endpointLocator.getTaskEndpoint().removeTaskByIndex(index, session);
        System.out.println("[TASK REMOVED]");
    }

}
