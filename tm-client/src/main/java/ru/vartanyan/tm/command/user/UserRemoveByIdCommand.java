package ru.vartanyan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractUserCommand;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "user-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove user by Id";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("REMOVE USER BY ID");
        System.out.println("[ENTER ID]");
        @NotNull final String userId = TerminalUtil.nextLine();
        endpointLocator.getAdminEndpoint().removeUserById(userId, session);
        System.out.println("[USER REMOVED]");
    }

}
