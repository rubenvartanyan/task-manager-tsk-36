package ru.vartanyan.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "show-version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[VERSION]");
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
        System.out.println(Manifests.read("build"));
    }

}
