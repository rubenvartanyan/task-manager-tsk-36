package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IAuthService;
import ru.vartanyan.tm.api.service.IUserService;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.empty.EmptyPasswordException;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.UserLockedException;
import ru.vartanyan.tm.exception.system.WrongRoleException;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService,
                       @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public User getUser() throws EmptyIdException {
        return userService.findById(userId);
    }

    @Nullable
    @Override
    public String getUserId() throws Exception {
        return userId;
    }

    @Override
    public boolean isNotAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@NotNull final String login,
                      @NotNull final String password) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        if (user.getLocked()) throw new UserLockedException();
        userId = user.getId();
    }

    @Override
    public void registry(@NotNull final String login,
                         @NotNull final String password,
                         @Nullable final String email) throws Exception {
        userService.create(login, password, email);
    }

    @Override
    public void checkRoles(@Nullable final Role... roles)
            throws WrongRoleException, NotLoggedInException, EmptyIdException {
        if (roles == null || roles.length == 0) return;
        final User user = getUser();
        if (user == null) throw new NotLoggedInException();
        @NotNull final Role role = user.getRole();
        for (final Role item: roles) {
            if (item.equals(role)) return;
            else throw new WrongRoleException();
        }
    }

}
