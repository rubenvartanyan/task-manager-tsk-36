package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.AbstractEntity;

import java.util.List;

public class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) return;
        repository.add(entity);
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Nullable
    @Override
    public E findById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(id);
    }

    @Nullable
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    public void clear() {
        repository.clear();
    }

    public void addAll(final List<E> list) {
        repository.addAll(list);
    }

    @Override
    public boolean contains(@NotNull String id) {
        return repository.contains(id);
    }

}
