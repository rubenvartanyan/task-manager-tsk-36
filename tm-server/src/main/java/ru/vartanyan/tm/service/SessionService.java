package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.ISessionRepository;
import ru.vartanyan.tm.api.service.ISessionService;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.exception.system.NoSuchUserException;
import ru.vartanyan.tm.exception.system.WrongPasswordException;
import ru.vartanyan.tm.exception.system.WrongRoleException;
import ru.vartanyan.tm.model.Session;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    private final ISessionRepository sessionRepository;

    public SessionService(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final ISessionRepository sessionRepository) {
        super(sessionRepository);
        this.serviceLocator = serviceLocator;
        this.sessionRepository = sessionRepository;
    }

    @Override
    public @Nullable Session open(final String login,
                                  final String password)
            throws NoSuchUserException, EmptyLoginException, WrongPasswordException, AccessDeniedException {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        List<User> userList = serviceLocator.getUserService().findAll();
        if (userList.size() == 0) {
            System.out.println("нет юзеров");
        }
        final @NotNull User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new NoSuchUserException();
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Override
    public void validate(@Nullable Session session,
                         @Nullable Role role) throws Exception {
        if (role == null) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        final String signatureSource = session.getSignature();
        final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();

    }

    @Override
    public void validateAdmin(@Nullable Session session,
                              @Nullable Role role) throws EmptyIdException, WrongRoleException, AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if ((session.getUserId()).isEmpty()) throw new AccessDeniedException();
        validate(session);
        final @NotNull Optional<User> user = Optional.ofNullable(serviceLocator.getUserService().findById(session.getUserId()));
        if (!user.isPresent()) throw new AccessDeniedException();
        if (user.get().getRole() != Role.ADMIN) throw new WrongRoleException();
    }

    @Override
    public @Nullable Session close(@Nullable Session session) throws Exception {
        sessionRepository.removeById(session.getId());
        return session;
    }



    @Override
    public boolean checkDataAccess(@NotNull String login,
                                   @NotNull String password) throws NoSuchUserException, EmptyLoginException, WrongPasswordException {
        if (login.isEmpty()) return false;
        if (password.isEmpty()) return false;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new NoSuchUserException();
        final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        boolean check = passwordHash.equals(user.getPasswordHash());
        if (!check) throw new WrongPasswordException();
        return (true);
    }

    @Override
    public Session sign(Session session) {
        if (session == null) return null;
        session.setSignature(null);
        final IPropertyService propertyService = serviceLocator.getPropertyService();
        final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

}
