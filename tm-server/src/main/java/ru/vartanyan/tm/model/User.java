package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Role;

@Setter
@Getter
@NoArgsConstructor
public class User extends AbstractEntity{

    @Nullable private String login;

    @Nullable private String passwordHash;

    @Nullable private String email;

    @Nullable private String firstName;

    @Nullable private String lastName;

    @Nullable private String middleName;

    @NotNull private Role role = Role.USER;

    @NotNull private Boolean locked = false;

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", role=" + role +
                ", role=" + id +
                ", locked=" + locked +
                '}';
    }
}
