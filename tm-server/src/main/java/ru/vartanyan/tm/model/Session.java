package ru.vartanyan.tm.model;


import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="yourRootElementName")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public final class Session extends AbstractEntity implements Cloneable {

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Nullable Long timestamp;

    @Nullable String userId;

    @Nullable String signature;

    public Session() {
    }

}