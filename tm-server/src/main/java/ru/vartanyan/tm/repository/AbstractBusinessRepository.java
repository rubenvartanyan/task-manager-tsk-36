package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    @NotNull
    @Override
    public final List<E> findAll(@NotNull final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public final E findById(@NotNull final String id,
                      @NotNull final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public void clear(@NotNull final String userId) {
        Iterator<E> iter  = entities.iterator();
        while (iter.hasNext()) {
            E item = iter.next();
            if (item.getUserId().equals(userId))
                iter.remove();
        }
    }

    @Override
    public void removeById(@NotNull final String id,
                           @NotNull final String userId) {
        entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
    }

    @Nullable
    @Override
    public final E findOneByIndex(@NotNull final Integer index,
                            @NotNull final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList())
                .get(index);
    }

    @Nullable
    @Override
    public final E findOneByName(@NotNull final String name,
                           @NotNull final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index,
                                 @NotNull final String userId) {
        remove(entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList())
                .get(index)
        );
    }

    @Override
    public void removeOneByName(@NotNull final String name,
                                @NotNull final String userId) {
        remove(entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null)
        );
    }

    @NotNull
    @Override
    public final List<E> findAll(@NotNull final Comparator<E> comparator,
                                          @NotNull final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    public void showEntity(@NotNull final E entity) {
        System.out.println("[NAME: " + entity.getName() + " ]");
        System.out.println("[DESCRIPTION: " + entity.getDescription() + " ]");
        System.out.println("STATUS: " + entity.getStatus() + " ]");
    }

}
