package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.ISessionRepository;
import ru.vartanyan.tm.model.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
}
