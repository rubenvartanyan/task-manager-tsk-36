package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public final List<Task> findAllByProjectId(@NotNull final String projectId,
                                                        @NotNull final String userId) {
        @Nullable final List<Task> listOfTask = new ArrayList<>();
        for (@Nullable final Task task: entities){
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) listOfTask.add(task);
        }
        return listOfTask;
    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId,
                                     @NotNull final String userId) {
        for (@Nullable final Task task: entities) {
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) remove(task);
        }
    }

    @Override
    public final void bindTaskByProjectId(@NotNull final String projectId,
                                          @NotNull final String taskId,
                                          @NotNull final String userId) {
        @Nullable final Task task = findById(taskId, userId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public final void unbindTaskFromProject(@NotNull final String projectId,
                                             @NotNull final String taskId,
                                             @NotNull final String userID) {
        @Nullable final Task task = findById(taskId, userID);
        if (task == null) return;
        task.setProjectId(null);
    }

}
