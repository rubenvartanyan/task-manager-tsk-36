package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.model.User;

import java.util.List;

public interface IUserService {

    @Nullable
    User findById(@NotNull final String id) throws EmptyIdException;

    void add(@NotNull final User user);

    @Nullable
    User findByLogin(@NotNull final String login) throws EmptyLoginException;

    void remove(@NotNull final User user);

    void removeById(@NotNull final String id) throws Exception;

    void removeByLogin(@NotNull final String login) throws Exception;

    User create(@NotNull final String login,
                @NotNull final String password) throws Exception;

    void create(@NotNull final String login,
                @NotNull final String password,
                @NotNull final String email) throws Exception;

    User create(@NotNull final String login,
                @NotNull final String password,
                @NotNull final Role role) throws Exception;

    void setPassword(@NotNull final String userId,
                     @NotNull final String password) throws Exception;

    void updateUser(@NotNull final String userId,
                    @Nullable final String firstName,
                    @Nullable final String lastName,
                    @Nullable final String middleName) throws Exception;

    void unlockUserByLogin(@NotNull final String login) throws Exception;

    void unlockUserById(@NotNull final String id) throws Exception;

    void lockUserByLogin(@NotNull final String login) throws Exception;

    void lockUserById(@NotNull final String id) throws Exception;

    @Nullable
    List<User> findAll();

    void clear();

    void addAll(final List<User> users);

}
