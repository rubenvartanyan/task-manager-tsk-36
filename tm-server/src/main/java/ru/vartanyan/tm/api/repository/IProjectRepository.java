package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IBusinessRepository<Project> {

}
