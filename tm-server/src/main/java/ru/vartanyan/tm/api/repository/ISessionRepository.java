package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {
}
