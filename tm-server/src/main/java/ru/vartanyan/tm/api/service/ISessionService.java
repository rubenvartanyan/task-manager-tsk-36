package ru.vartanyan.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.exception.system.NoSuchUserException;
import ru.vartanyan.tm.exception.system.WrongPasswordException;
import ru.vartanyan.tm.exception.system.WrongRoleException;
import ru.vartanyan.tm.model.Session;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session open(String login, String password)
            throws NoSuchUserException, EmptyLoginException, WrongPasswordException, AccessDeniedException;

    @SneakyThrows
    void validate(@Nullable Session session,
                  @Nullable Role role) throws Exception;

    @SneakyThrows
    void validate(@Nullable Session session) throws Exception;

    @SneakyThrows
    void validateAdmin(@Nullable Session session,
                       @Nullable Role role) throws EmptyIdException, WrongRoleException, AccessDeniedException;

    @Nullable Session close(@Nullable Session session) throws Exception;

    boolean checkDataAccess(@NotNull final String login,
                            @NotNull final String password) throws Exception;

    Session sign(final Session session);

}
