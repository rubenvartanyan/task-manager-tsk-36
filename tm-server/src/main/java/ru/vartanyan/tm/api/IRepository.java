package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(@NotNull final E entity);

    void remove(@NotNull final E entity);

    @Nullable
    E findById(@NotNull final String id) throws EmptyIdException;

    void removeById(@NotNull final String id) throws Exception;

    @Nullable
    List<E> findAll();

    void clear();

    void addAll(final List<E> entities);

    boolean contains(@NotNull String id);

}
