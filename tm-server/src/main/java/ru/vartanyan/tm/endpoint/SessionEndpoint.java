package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.endpoint.ISessionEndpoint;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.exception.system.NoSuchUserException;
import ru.vartanyan.tm.exception.system.WrongPasswordException;
import ru.vartanyan.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Override
    public Session openSession(@WebParam(name = "login", partName = "login") String login,
                               @WebParam(name = "password", partName = "password") String password
    ) throws AccessDeniedException, EmptyLoginException, NoSuchUserException, WrongPasswordException {
        return serviceLocator.getSessionService().open(login, password);
    }

    @WebMethod
    @Override
    public List<Session> listSession() {
        return serviceLocator.getSessionService().findAll();
    }

    @WebMethod
    @Nullable
    @Override
    public Session closeSession(@WebParam(name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().close(session);
    }

}
