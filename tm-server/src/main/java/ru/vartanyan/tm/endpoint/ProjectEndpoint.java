package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.endpoint.IProjectEndpoint;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    @Override
    public Project findProjectById(@WebParam (name = "id", partName = "id") @NotNull String id,
                                   @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(id, session.getUserId());
    }

    @WebMethod
    @Override
    public void removeProjectById(@WebParam (name = "id", partName = "id") @NotNull String id,
                                  @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeById(id, session.getUserId());
    }

    @WebMethod
    @NotNull
    @Override
    public List<Project> findAllProjects(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @WebMethod
    @Override
    public void clearProjects(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @Nullable
    @WebMethod
    @Override
    public Project findProjectByIndex(@WebParam (name = "index", partName = "index") @NotNull Integer index,
                                      @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByIndex(index, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public Project findProjectByName(@WebParam (name = "name", partName = "name") @NotNull String name,
                                     @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByName(name, session.getUserId());
    }

    @Override
    public void removeProjectByIndex(@WebParam (name = "index", partName = "index") @NotNull Integer index,
                                     @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeOneByIndex(index, session.getUserId());
    }

    @WebMethod
    @Override
    public void removeProjectByName(@WebParam (name = "name", partName = "name") @NotNull String name,
                                    @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeOneByName(name, session.getUserId());
    }

    @WebMethod
    @Override
    public void addProject(@WebParam (name = "name", partName = "name") @NotNull String name,
                           @WebParam (name = "description", partName = "description") @NotNull String description,
                           @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().add(name, description, session.getUserId());

    }
}
