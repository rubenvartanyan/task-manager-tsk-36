package ru.vartanyan.tm.exception.system;

public class UserLockedException extends Exception{

    public UserLockedException() {
        super("Error! Your account is locked...");
    }

}
