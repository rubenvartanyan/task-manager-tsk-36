package ru.vartanyan.tm.exception.system;

public class WrongRoleException extends Throwable {

    public WrongRoleException() {
        super("Error! You role doesn't support this command");
    }

}
