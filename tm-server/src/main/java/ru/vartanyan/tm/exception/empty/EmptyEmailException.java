package ru.vartanyan.tm.exception.empty;

public class EmptyEmailException extends Exception{

    public EmptyEmailException() {
        super("Error! Email cannot be null or empty...");
    }

}
