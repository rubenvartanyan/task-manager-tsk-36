package ru.vartanyan.tm.exception.system;

public class NoSuchUserException extends Exception{

    public NoSuchUserException()  {
        super("Error! No such user...");
    }

}
