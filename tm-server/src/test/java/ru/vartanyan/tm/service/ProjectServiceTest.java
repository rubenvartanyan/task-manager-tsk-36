package ru.vartanyan.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.service.IProjectService;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.repository.ProjectRepository;
import ru.vartanyan.tm.service.ProjectService;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void addAllProjectsTest(){
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertNotNull(projectService.findById(project1.getId()));
        Assert.assertNotNull(projectService.findById(project2.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void addProjectTest() throws Exception {
        final Project project = new Project();
        projectService.add(project);
        Assert.assertNotNull(projectService.findById(project.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearProjectsTest() {
        projectService.clear();
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllProjects() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertEquals(2, projectService.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIdTest() throws Exception {
        final Project project = new Project();
        final String projectId = project.getId();
        projectService.add(project);
        Assert.assertNotNull(projectService.findById(projectId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIndexTest() throws Exception {
        projectService.clear();
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectService.add(project);
        Assert.assertEquals(project, projectService.findOneByIndex(0, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByNameTest() throws Exception {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("project1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Assert.assertEquals(project, projectService.findOneByName(name, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIdTest() throws Exception {
        final Project project1 = new Project();
        projectService.add(project1);
        final String projectId = project1.getId();
        projectService.removeById(projectId);
        Assert.assertNull(projectService.findById(projectId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByNameTest() throws Exception {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("project1");
        final String name = project.getName();
        projectService.add(project);
        projectService.removeOneByName(name, userId);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIndexTest() throws Exception {
        projectService.clear();
        final Project project = new Project();
        final String projectId = project.getId();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("project1");
        final String name = project.getName();
        projectService.add(project);
        projectService.removeOneByIndex(0, userId);
        Assert.assertNull(projectService.findById(name, projectId));
    }

}
