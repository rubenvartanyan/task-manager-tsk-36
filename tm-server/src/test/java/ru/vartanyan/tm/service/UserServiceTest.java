package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.api.service.IUserService;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.repository.UserRepository;
import ru.vartanyan.tm.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @Test
    @Category(UnitCategory.class)
    public void addAllUsersTest() throws Exception {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userService.addAll(users);
        Assert.assertNotNull(userService.findById(user1.getId()));
        Assert.assertNotNull(userService.findById(user2.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void addUserTest() throws Exception {
        final User user = new User();
        userService.add(user);
        Assert.assertNotNull(userService.findById(user.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearUsersTest() {
        userService.clear();
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllUsers() {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userService.addAll(users);
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findUserByIdTest() throws Exception {
        final User user = new User();
        final String userId = user.getId();
        userService.add(user);
        Assert.assertNotNull(userService.findById(userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeUserByIdTest() throws Exception {
        final User user1 = new User();
        userService.add(user1);
        final String userId = user1.getId();
        userService.removeById(userId);
        Assert.assertNull(userService.findById(userId));
    }

}
