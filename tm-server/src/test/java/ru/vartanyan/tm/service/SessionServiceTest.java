package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.repository.ISessionRepository;
import ru.vartanyan.tm.api.service.ISessionService;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Session;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.repository.SessionRepository;
import ru.vartanyan.tm.service.SessionService;

import java.util.ArrayList;
import java.util.List;

public class SessionServiceTest {

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(serviceLocator, sessionRepository);

    @Test
    @Category(UnitCategory.class)
    public void addAllSessionsTest() throws Exception {
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertNotNull(sessionService.findById(session1.getId()));
        Assert.assertNotNull(sessionService.findById(session2.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void addSessionTest() throws Exception {
        final Session session = new Session();
        sessionService.add(session);
        Assert.assertNotNull(sessionService.findById(session.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearSessionsTest() {
        sessionService.clear();
        Assert.assertTrue(sessionService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllSessions() {
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertEquals(2, sessionService.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findSessionByIdTest() throws Exception {
        final Session session = new Session();
        final String sessionId = session.getId();
        sessionService.add(session);
        Assert.assertNotNull(sessionService.findById(sessionId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeSessionByIdTest() throws Exception {
        final Session session1 = new Session();
        sessionService.add(session1);
        final String sessionId = session1.getId();
        sessionService.removeById(sessionId);
        Assert.assertNull(sessionService.findById(sessionId));
    }

}
