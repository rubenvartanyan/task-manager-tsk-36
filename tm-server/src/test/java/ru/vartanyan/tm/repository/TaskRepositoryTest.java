package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Test
    @Category(UnitCategory.class)
    public void addAllTasksTest() throws Exception {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskRepository.addAll(tasks);
        Assert.assertNotNull(taskRepository.findById(task1.getId()));
        Assert.assertNotNull(taskRepository.findById(task2.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void addTaskTest() throws Exception {
        final Task task = new Task();
        taskRepository.add(task);
        Assert.assertNotNull(taskRepository.findById(task.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTasksTest() {
        taskRepository.clear();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTasks() {
        taskRepository.clear();
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskRepository.addAll(tasks);
        Assert.assertEquals(2, taskRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findTaskByIdTest() throws Exception {
        taskRepository.clear();
        final Task task = new Task();
        final String taskId = task.getId();
        taskRepository.add(task);
        Assert.assertNotNull(taskRepository.findById(taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findTaskByIndexTest() throws Exception {
        taskRepository.clear();
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        taskRepository.add(task);
        Assert.assertEquals(task, taskRepository.findOneByIndex(0, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findTaskByNameTest() throws Exception {
        taskRepository.clear();
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        taskRepository.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        Assert.assertEquals(task, taskRepository.findOneByName(name, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByIdTest() throws Exception {
        taskRepository.clear();
        final Task task1 = new Task();
        taskRepository.add(task1);
        final String taskId = task1.getId();
        taskRepository.removeById(taskId);
        Assert.assertNull(taskRepository.findById(taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByNameTest() throws Exception {
        taskRepository.clear();
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        final String name = task.getName();
        taskRepository.add(task);
        taskRepository.removeOneByName(name, userId);
        Assert.assertNull(taskRepository.findOneByName(name, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByIndexTest() throws Exception {
        taskRepository.clear();
        final Task task = new Task();
        final String taskId = task.getId();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        final String name = task.getName();
        taskRepository.add(task);
        taskRepository.removeOneByIndex(0, userId);
        Assert.assertNull(taskRepository.findById(name, taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTasksByProjectIdTest() {
        taskRepository.clear();
        final User user = new User();
        final String userId = user.getId();
        final Project project = new Project();
        final Task task1 = new Task();
        final Task task2 = new Task();
        final String projectId = project.getId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task1.setProjectId(projectId);
        task2.setProjectId(projectId);
        taskRepository.add(task1);
        taskRepository.add(task2);
        final List<Task> taskList = taskRepository.findAllByProjectId(projectId, userId);
        Assert.assertEquals(2, taskList.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeAllByProjectIdTest() {
        taskRepository.clear();
        final User user = new User();
        final String userId = user.getId();
        Project project = new Project();
        Task task1 = new Task();
        task1.setProjectId(project.getId());
        task1.setUserId(userId);
        Task task2 = new Task();
        task2.setProjectId(project.getId());
        task2.setUserId(userId);
        taskRepository.removeAllByProjectId(project.getId(), userId);
        final long size = taskRepository.findAllByProjectId(project.getId(), userId).size();
        Assert.assertEquals(0, size);
    }

    @Test
    @Category(UnitCategory.class)
    public void bindTaskByProjectIdTest() {
        taskRepository.clear();
        final User user = new User();
        final String userId = user.getId();
        final Project project = new Project();
        project.setUserId(userId);
        final String projectId = project.getId();
        final Task task = new Task();
        task.setUserId(userId);
        final String taskId = task.getId();
        taskRepository.add(task);
        taskRepository.bindTaskByProjectId(projectId, taskId, userId);
        List<Task> taskList = new ArrayList<>();
        taskList.add(task);
        Assert.assertEquals(taskList, taskRepository.findAllByProjectId(projectId, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void unbindTaskFromProjectTest() {
        taskRepository.clear();
        final User user = new User();
        final String userId = user.getId();
        final Project project = new Project();
        project.setUserId(userId);
        final String projectId = project.getId();
        final Task task = new Task();
        task.setUserId(userId);
        final String taskId = task.getId();
        taskRepository.add(task);
        taskRepository.bindTaskByProjectId(projectId, taskId, userId);
        List<Task> taskList = new ArrayList<>();
        taskList.add(task);
        Assert.assertEquals(taskList, taskRepository.findAllByProjectId(projectId, userId));
        taskRepository.unbindTaskFromProject(projectId, taskId, userId);
        Assert.assertNull(task.getProjectId());
    }

}
