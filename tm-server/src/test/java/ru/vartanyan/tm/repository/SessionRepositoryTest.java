package ru.vartanyan.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.repository.ISessionRepository;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Session;
import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void addAllSessionsTest() {
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionRepository.addAll(sessions);
        Assert.assertNotNull(sessionRepository.findById(session1.getId()));
        Assert.assertNotNull(sessionRepository.findById(session2.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void addSessionTest() throws Exception {
        final Session session = new Session();
        sessionRepository.add(session);
        Assert.assertNotNull(sessionRepository.findById(session.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearSessionsTest() {
        sessionRepository.clear();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllSessions() {
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionRepository.addAll(sessions);
        Assert.assertEquals(2, sessionRepository.findAll().size());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void findSessionByIdTest() {
        final Session session = new Session();
        final String sessionId = session.getId();
        sessionRepository.add(session);
        Assert.assertNotNull(sessionRepository.findById(sessionId));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void removeSessionByIdTest() {
        final Session session1 = new Session();
        sessionRepository.add(session1);
        final String sessionId = session1.getId();
        sessionRepository.removeById(sessionId);
        Assert.assertNull(sessionRepository.findById(sessionId));
    }

}
