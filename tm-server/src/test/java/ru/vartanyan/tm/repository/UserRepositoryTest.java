package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Test
    @Category(UnitCategory.class)
    public void addAllUsersTest() throws Exception {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userRepository.addAll(users);
        Assert.assertNotNull(userRepository.findById(user1.getId()));
        Assert.assertNotNull(userRepository.findById(user2.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void addUserTest() throws Exception {
        final User user = new User();
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findById(user.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearUsersTest() {
        userRepository.clear();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllUsers() {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userRepository.addAll(users);
        Assert.assertEquals(2, userRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findUserByIdTest() throws Exception {
        final User user = new User();
        final String userId = user.getId();
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findById(userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeUserByIdTest() throws Exception {
        final User user1 = new User();
        userRepository.add(user1);
        final String userId = user1.getId();
        userRepository.removeById(userId);
        Assert.assertNull(userRepository.findById(userId));
    }

}
